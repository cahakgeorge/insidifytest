<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix'=>'v1',
    'namespace'=>'V1',
    ],function(){

    Route::POST('login', 'AuthController@authenticate');//->middleware('throttle');

    Route::POST('signup', 'UserController@registerUser');//->middleware('throttle');
    
    Route::group(['middleware' => 'auth:api'], function(){

        Route::prefix('auths')->group(function(){
            //logout user
            Route::GET('logout', 'AuthController@logout');
            //Auth middleware redirects failed authentication here
            Route::GET('login', [ 'as' => 'login'], '');
        });

        Route::prefix('courses')->group(function(){

            Route::PUT('', 'UserController@generateCourses');//->middleware('throttle');

            Route::POST('registration', 'UserController@registerForCourse');//->middleware('throttle');
    
            Route::GET('all', 'UserController@getCourseList');
    
            Route::GET('exports', 'UserController@exportAllCourses');
        });
    

    });

});

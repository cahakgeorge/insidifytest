<?php

namespace App\Exceptions;

use Exception;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Support\Facades\Log;

//LINE 78 render function switches responses between api calls and web route calls
//https://laracasts.com/discuss/channels/requests/custom-exception-handler-based-on-route-group
use Symfony\Component\HttpFoundation\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>'Unauthorized', //$exception->getMessage()
            ], Response::HTTP_UNAUTHORIZED);
        }
        
        //upload max exceeded
        if($exception instanceof  \Illuminate\Http\Exceptions\PostTooLargeException ){
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>'Filesize too large',
            ], Response::HTTP_REQUEST_ENTITY_TOO_LARGE);//Response::HTTP_BAD_REQUEST
        }

        //database field type restriction exceeded
        if($exception instanceof  \Illuminate\Database\QueryException ){
            Log::critical("*Db error:* ".json_encode($exception->getMessage()));
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>'something went wrong!',
                // 'message'=>"Query Error"//'Db Error', /$exception->getMessage() : 
            ], Response::HTTP_PRECONDITION_FAILED);
        }
        
        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=>'unauthenticated',
            ], Response::HTTP_UNAUTHORIZED);//
        }
        
        $namespacedClassName = get_class($exception);
        $message = $exception->getMessage();

        if($request->is('api/*')){
            //Log::critical("*Api error:* ".json_encode($message).' - '.json_encode($namespacedClassName));
            
            if(stripos($message, 'sql') !== false) $msg = 'Bad Request';
            //else if(stripos($message, 'unauthenticate') !== false) $msg = 'User not logged in!';
            else $msg = $exception->getMessage();

            return response()->json([
                'status'=>false,
                'data'=>null,
                'message'=> $msg//;(stripos($message, 'sql') !== false) ? 'Bad Request' : $message //$exception->getMessage()
            ], Response::HTTP_BAD_REQUEST);//Response::HTTP_BAD_REQUEST  //Symfony\\Component\\HttpKernel\\Exception\\HttpException
        }
        
        return parent::render($request, $exception);
    }
}

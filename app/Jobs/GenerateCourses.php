<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Course;

class GenerateCourses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

     /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 10;

    public $numberOfCourses;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $courseNo)
    {
        $this->numberOfCourses = $courseNo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $courseFactory = factory(Course::class, $this->numberOfCourses)->create();
    }
}

<?php

namespace App\Helpers;

/**
 * @group User management
 *
 * APIs for managing users
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

//use Event;
use Carbon\Carbon;

use App\Jobs\{
    GenerateCourses
};

use App\{
        Course, User
};

use Event;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;


class UserHelper
{

    public function __construct()
	{
	}
    
    /**
     * Signup user
     */
    public function registerUser($request): bool
	{  
        User::create(['email'=>$request->email, 'name'=>$request->first_name.' '.$request->last_name, 'email_verified_at'=>now(), 
                        'password'=>bcrypt($request->password), 'created_at'=>now()]);
        
        //do something else, like send an email link
        return true;
    }
    
}
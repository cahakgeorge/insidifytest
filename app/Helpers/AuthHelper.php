<?php

namespace App\Helpers;

/**
 * @group User management
 *
 * APIs for managing users
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

//use Event;
use Carbon\Carbon;

use Event;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

/* use App\Traits\{
    AuthFunctions, UserPermissions
};
 */


class AuthHelper
{
    //use AuthFunctions, UserPermissions;
    
    protected $userRepository;

    public function __construct()
	{
	}
    
    /**
     * Authenticate user on platform
     * @bodyParam email string required The users email
     * @bodyParam password string required The users password
     */
    public function authenticateUser($request): array
	{   
        if(!Auth::guard('web')->attempt($request->only('email', 'password') )) 
            throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>"Email or password incorrect"], 412));
        
        $user = Auth::guard('web')->user();

        if(empty($user->email_verified_at))
            throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>'Account not verified. Kindly click on the link in your email'], 412));
        
        $userAccessToken = $this->getAccessTokenForUser($user);
        
        return ['token'=> $userAccessToken, 'name'=>$user->name];                 
    }

     /**
     * Get access token for user
     */
    public function getAccessTokenForUser($user): string{
        //get token
        $passportAppName = env('PASSPORT_TOKEN_NAME', 'default');
        $tokenResult = $user->createToken($passportAppName);

        if(!$tokenResult) throw new HttpResponseException(response()->json(["status"=>false,"data"=>null,"message"=>"No created token"], 412));

        $token = $tokenResult->token;
        
        $token->save();

        return $tokenResult->accessToken; //could also retrieve the refresh token for the access token generated here as well
    }

    
    /**
     * Logout user from platform
     */
	public function logoutUser($request): bool
	{ //validated data present
        return $request->user()->token()->revoke();
    }

}
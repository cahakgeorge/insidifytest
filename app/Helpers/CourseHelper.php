<?php

namespace App\Helpers;

/**
 * @group User management
 *
 * APIs for managing users
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

//use Event;
use Carbon\Carbon;

use App\Jobs\{
    GenerateCourses
};

use App\{
        Course, UserHasCourse
};

use Event;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

use App\Exports\CoursesExport;
use Maatwebsite\Excel\Facades\Excel;


class CourseHelper
{
    

    public function __construct()
	{
	}
    
    /**
     * Call job to generate courses
     */
    public function generateCourses(): bool
	{   
        //send token message //mail welcome email view to user
        dispatch(new GenerateCourses($number = 50))->delay(Carbon::now()->addSeconds(4)); //run 4 seconds later
    
        return true;
    }

    /**
     * Get course list
     */
    public function getCourseList(): object
	{   
        $user = Auth::user(); //authenticated user

        $courseList = Course::selectRaw("courses.id, courses.text,
                                        (SELECT CASE
                                                WHEN user_has_course.user_id IS NOT NULL THEN courses.unit
                                                ELSE '0'
                                                END as category) AS course_unit,
                                        (SELECT CASE
                                                WHEN user_has_course.user_id IS NOT NULL THEN courses.code
                                                ELSE '0'
                                                END as category) AS course_code 
                            ")
                            ->leftJoin('user_has_course', function($q) use($user){
                                $q->on('user_has_course.user_id', DB::raw("'$user->id'"));
                                $q->on('user_has_course.course_id', 'courses.id');
                            })->get();

        return $courseList;
    }

    /**
     * Call job to generate courses
     */
    public function registerForCourse(object $request): bool
	{   
        $user = Auth::user(); //authenticated user

        UserHasCourse::create(['course_id'=>$request->course_id, 'user_id'=>$user->id]);
        
        return true;
    }

    /**
     * Export all caourses
     */
    public function exportAllCourses()
	{   
        return Excel::download(new CoursesExport, 'courses-'.Carbon::now().'.xlsx');
    }

    
}
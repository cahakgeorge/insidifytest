<?php

namespace App\Http\Requests;

use Exception;

class RegisterUser extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|max:255|unique:users,email',
            'first_name' => 'bail|required|alpha_dash|max:100',
            'last_name' => 'bail|required|alpha_dash|max:100',
            'password' => 'bail|required|string|between:4,100',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'integer'      => 'The :attribute must have an integer value',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters(): array
    {
        return [
        ];
    }   
}

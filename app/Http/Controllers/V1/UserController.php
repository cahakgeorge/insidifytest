<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Exceptions\HttpResponseException;


use App\Http\Requests\{
    RegisterForCourse, RegisterUser
};



use CourseHelper;
use UserHelper;
use App\Http\Controllers\V1\Contracts\UserControllerInterface;

/**
 * @group User Functions
 *
 * APIs for managing user functions
 */

class UserController extends Controller implements UserControllerInterface
{
     /**
     * Register user
     * @unauthenticated
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function registerUser(RegisterUser $request)
    {
        $userHelper = new UserHelper();   
        if($userHelper->registerUser($request)) return response()->json(['status'=>true, 'data'=>null, 'message'=>null], Response::HTTP_OK);
    }

    /**
     * Create courses
     * @unauthenticated
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function generateCourses()
    {
        $courseHelper = new CourseHelper();   
        if($courseHelper->generateCourses()) return response()->json(['status'=>true, 'data'=>null, 'message'=>null], Response::HTTP_OK);
    }

    /**
     * Get list of courses
     * @unauthenticated
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function getCourseList()
    {
        $courseHelper = new CourseHelper();   
        if(($data = $courseHelper->getCourseList()))
                        return response()->json(['status'=>true, 'data'=>$data, 'message'=>null], Response::HTTP_OK);
    }


    /**
     * Register for course
     * @bodyParam course_id string required The course id to be registered for
     * @response {
     *       "status": true,
     *       "data": null,
     *       "message": null
     *  }
     */
    public function registerForCourse(RegisterForCourse $request)
    {
        $courseHelper = new CourseHelper();
        if($courseHelper->registerForCourse($request)) return response()->json(['status'=>true, 'data'=>null, 'message'=>null], Response::HTTP_OK);
    }

    /**
     * Export courses
     */
    public function exportAllCourses()
    {
        $courseHelper = new CourseHelper();
        return $courseHelper->exportAllCourses();
    }

}

<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


use AuthHelper;
use App\Http\Requests\{
                        AuthUserRequest
                    };


/**
 * 
 * 
 * @group User authentication
 *
 * APIs for managing authentication, and resets
 */
                                                           
class AuthController extends Controller
{
    
    public function __construct()
	{
        $this->authHelper = new AuthHelper(); 
    }
    
    /**
     * Authenticate User Login
     * via credentials entered on platform
     * @bodyParam email string required The users email
     * @bodyParam password string required The users password
     * @response {
     * "status": true,
     *  "data": {
     *               "token": "$token",
     *               "category": "$category",
     *               "name": "$name"
     *           },
     *  "message": null
     * }
	 */
    public function authenticate(AuthUserRequest $request)
    {   //Validated data present
        if(($userData = $this->authHelper->authenticateUser($request))) 
                                                             return response()->json(['status'=>true, 'data'=>$userData, 'message'=>null], Response::HTTP_OK);
    }
    
    
    /**
     * Logout user
     * Logout user and revoke access token
     * @response {
     *      "status": true,
     *      "data": null,
     *      "message": ''
     * }
     * * @return string 
     */
    public function logout(Request $request)
    {
        $status = $this->authHelper->logoutUser($request);
        return response()->json(['status'=>$status, 'data'=>null, 'message'=>(!empty($status)) ? '' : 'Logout failed'], 
                                    Response::HTTP_OK);
    }
}

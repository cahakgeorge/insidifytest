<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasCourse extends Model
{

    protected $table = 'user_has_course';
    protected $primaryKey = 'id';

    public  $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'course_id', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $date = [
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }

    public function course()
    {
        return $this->belongsTo('App\Course', 'id');
    }

}
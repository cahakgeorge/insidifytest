<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCoursesTable extends Migration
{

    public $tableName = 'courses';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable($this->tableName)) {

            Schema::table($this->tableName, function (Blueprint $table) {
                $table->string('code', 200)->nullable()->comment('Course code');
                $table->tinyInteger('unit')->unsigned()->nullable()->comment('course unit');
                $table->timestamp('expiry_date')->nullable()->comment('Course expiry date');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->dropColumn('unit');
            $table->dropColumn('expiry_date');
        });
    }
}

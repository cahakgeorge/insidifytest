<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCourseTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user_has_course';

    /**
     * Run the migrations.
     * @table user_has_course
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->comment('Id of user');
            $table->bigInteger('course_id')->unsigned()->comment('Course Id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));


            /* $table->foreign('user_id')
                ->references('id')->on('user')
                ->onDelete('cascade')
                ->onUpdate('restrict');

            $table->foreign('course_id')
                ->references('id')->on('course')
                ->onDelete('cascade')
                ->onUpdate('restrict'); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
